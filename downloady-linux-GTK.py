from gi.repository import Gtk
import os

class PyApp(Gtk.Window):

    def __init__(self):
        super(PyApp, self).__init__()
        self.set_title("Downloady in GTK!")
        self.set_default_size(500,400)

        self.label = Gtk.Label("Enter URL")
        self.entry = Gtk.Entry()
        
        self.label2 = Gtk.Label("Enter Path")
        self.entry_path = Gtk.Entry()

        self.btn = Gtk.Button("Video & Audio")
        self.btn.connect("clicked", self.Video_and_audio)

        self.btn2 = Gtk.Button("Audio Only")
        self.btn2.connect("clicked", self.Only_audio)

        self.btn3 = Gtk.Button("Close")
        self.btn3.connect("clicked", self.Close_window)
        self.status = Gtk.Label("Status: -")

        #                                                [Puts all the objects]
        fixed = Gtk.Fixed()
        fixed.put(self.label, 100, 50)#                 <--------------------
        fixed.put(self.entry, 100, 75)
        fixed.put(self.label2, 100, 125)
        fixed.put(self.entry_path, 100, 150)
        fixed.put(self.btn, 100, 200)
        fixed.put(self.btn2, 220, 200)
        fixed.put(self.btn3, 320, 200)
        fixed.put(self.status, 100, 250)

        self.add(fixed)
        self.show_all()
    
    def Only_audio(self, button):
        self.status_confirmation = True
        self.name = self.entry.get_text()
        self.path = self.entry_path.get_text()
        if self.btn2.connect("clicked", self.Only_audio):
            print("\nFunction Only_audio used\n")
            self.Only_audio_motor()
            return self.status.set_text("Status: DONE")
    def Only_audio_motor(self):
        os.system(f"youtube-dl -o '{self.path}%(title)s.%(ext)s' -x --audio-format mp3 {self.name}")

    def Close_window(self, button):
        print("Aplication closed.")
        Gtk.main_quit()

    def Video_and_audio(self, button):
        self.name = self.entry.get_text()
        self.path = self.entry_path.get_text()
        if self.btn.connect("clicked", self.Video_and_audio):
            print("\nFunction Video_and_audio used\n")
            os.system(f"youtube-dl -o '{self.path}%(title)s.%(ext)s' {self.name}")
            print("Downloaded!")


PyApp()
Gtk.main()
